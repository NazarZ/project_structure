﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class ProjectService : BaseService,  IProjectService
    {
        public ProjectService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public void CreateProject(ProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            _context.Projects.Create(projectEntity);
        }

        public void DeleteProject(int idProject)
        {
            _context.Projects.Delete(idProject);
        }

        public void Dispose()
        {
            
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _mapper.Map<List<ProjectDTO>>(_context.Projects.GetAll());
        }

        public ProjectDTO GetProjectById(int id)
        {
            return _mapper.Map<ProjectDTO>(_context.Projects.Get(id));
        }

        public void UpdateProject(ProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);
            _context.Projects.Update(projectEntity);
        }
    }
}
