﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Structure.BLL.Services
{
    public class TasksService : BaseService, ITaskService
    {
        public TasksService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
         
        }
        public void CreateTask(TaskDTO task)
        {
            var taskEntity = _mapper.Map<Task>(task);
            _context.Tasks.Create(taskEntity);
        }

        public void DeleteTask(int idTask)
        {
            _context.Tasks.Delete(idTask);
        }

        public IEnumerable<TaskDTO> GetAllTasks()
        {
            return _mapper.Map<List<TaskDTO>>(_context.Tasks.GetAll());
        }

        public TaskDTO GetTaskById(int id)
        {
            return _mapper.Map<TaskDTO>(_context.Tasks.Get(id));
        }

        public void UpdateTask(TaskDTO task)
        {
            var taskEntity = _mapper.Map<Task>(task);
            _context.Tasks.Update(taskEntity);
        }
    }
}
