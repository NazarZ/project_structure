﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public void CreateTeam(TeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            _context.Teams.Create(teamEntity);
        }

        public void DeleteTeam(int idTask)
        {
            _context.Teams.Delete(idTask);
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {

            return _mapper.Map<IEnumerable<TeamDTO>>(_context.Teams.GetAll());
        }

        public TeamDTO GetTeamById(int id)
        {
            return _mapper.Map<TeamDTO>(_context.Teams.Get(id));
        }

        public void UpdateTeam(TeamDTO team)
        {
            var teamEntity = _mapper.Map<Team>(team);
            _context.Teams.Update(teamEntity);
        }
    }
}
