﻿using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IUserService
    {
        public IEnumerable<UserDTO> GetAllUsers();
        public UserDTO GetUserById(int id);
        public void CreateUser(UserDTO user);
        public void UpdateUser(UserDTO user);
        public void DeleteUser(int idTask);
    }
}
