﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Data;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Project_Structure.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private ProjectRepository _projects;
        private TaskRepository _tasks;
        private TeamRepository _teams;
        private UserRepository _users;

        public EFUnitOfWork()
        {
            Іnitializer.EFInit(this);
        }

        public IRepository<Project> Projects 
        {
            get
            {
                if(_projects is null)
                {
                    _projects = new();
                }
                return _projects;
            }
        }
        public IRepository<Task> Tasks
        {
            get
            {
                if (_tasks is null)
                {
                    _tasks = new();
                }
                return _tasks;
            }
        }
        public IRepository<Team> Teams
        {
            get
            {
                if (_teams is null)
                {
                    _teams = new();
                }
                return _teams;
            }
        }
        public IRepository<User> Users
        {
            get
            {
                if (_users is null)
                {
                    _users = new();
                }
                return _users;
            }
        }
    }
}
