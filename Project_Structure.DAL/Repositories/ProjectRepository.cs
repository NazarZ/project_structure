﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private List<Project> _projects;
        public ProjectRepository()
        {
            _projects = new();
        }

        public void Create(Project item)
        {
            _projects.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _projects.Find(x => x.Id == id);
            if (toRemove is not null)
            {
                _projects.Remove(toRemove);
            }
        }

        public IEnumerable<Project> Find(Func<Project, bool> predicate)
        {
            return _projects.Where(predicate).ToList();
        }

        public Project Get(int id)
        {
            return _projects.Find(x => x.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _projects;
        }

        public void Update(Project item)
        {
            var project = _projects.Find(x => x.Id == item.Id);
            if(project is not null)
            {
                project = item;
            }
        }
    }
}
