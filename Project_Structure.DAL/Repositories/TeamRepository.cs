﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private List<Team> _teams;
        public TeamRepository()
        {
            _teams = new();
        }

        public void Create(Team item)
        {
            _teams.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _teams.Find(x => x.Id == id);
            if (toRemove is not null)
            {
                _teams.Remove(toRemove);
            }
        }

        public IEnumerable<Team> Find(Func<Team, bool> predicate)
        {
            return _teams.Where(predicate).ToList();
        }

        public Team Get(int id)
        {
            return _teams.Find(x => x.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _teams;
        }

        public void Update(Team item)
        {
            var project = _teams.Find(x => x.Id == item.Id);
            if (project is not null)
            {
                project = item;
            }
        }
    }
}
