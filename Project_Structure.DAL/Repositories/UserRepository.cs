﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private List<User> _users;
        public UserRepository()
        {
            _users = new();
        }

        public void Create(User item)
        {
            _users.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _users.Find(x => x.Id == id);
            if (toRemove is not null)
            {
                _users.Remove(toRemove);
            }
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return _users.Where(predicate).ToList();
        }

        public User Get(int id)
        {
            return _users.Find(x => x.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public void Update(User item)
        {
            var project = _users.Find(x => x.Id == item.Id);
            if (project is not null)
            {
                project = item;
            }
        }
    }
}
