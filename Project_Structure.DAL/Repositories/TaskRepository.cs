﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Structure.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {
        private List<Task> _tasks;
        public TaskRepository()
        {
            _tasks = new();
        }

        public void Create(Task item)
        {
            _tasks.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _tasks.Find(x => x.Id == id);
            if (toRemove is not null)
            {
                _tasks.Remove(toRemove);
            }
        }

        public IEnumerable<Task> Find(Func<Task, bool> predicate)
        {
            return _tasks.Where(predicate).ToList();
        }

        public Task Get(int id)
        {
            return _tasks.Find(x => x.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _tasks;
        }

        public void Update(Task item)
        {
            var project = _tasks.Find(x => x.Id == item.Id);
            if (project is not null)
            {
                project = item;
            }
        }
    }
}
