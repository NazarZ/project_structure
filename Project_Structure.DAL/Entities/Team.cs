﻿using System;

namespace Project_Structure.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
