﻿using Project_Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Structure.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<Task> Tasks { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }
    }
}
