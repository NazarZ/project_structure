﻿using Microsoft.Extensions.DependencyInjection;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.MappingProfiles;
using Project_Structure.BLL.Services;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using Project_Structure.WebAPI.Converters;
using System.Reflection;

namespace Project_Structure.API.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddSingleton<IUnitOfWork, EFUnitOfWork>();
            services.AddTransient<IQueryService, QueryService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TasksService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();

            //services.AddControllers()
            //        .AddJsonOptions(options =>
            //        {
            //            options.JsonSerializerOptions.Converters.Add(new DictionaryInt32Converter());
            //        });
        }
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
